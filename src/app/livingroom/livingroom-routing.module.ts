import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LivingroomComponent } from './livingroom/livingroom.component';

const routes: Routes = [
  {
    path:"livingroom",
    component: LivingroomComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LivingroomRoutingModule { }
