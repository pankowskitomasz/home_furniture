import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivingroomS1Component } from './livingroom-s1.component';

describe('LivingroomS1Component', () => {
  let component: LivingroomS1Component;
  let fixture: ComponentFixture<LivingroomS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivingroomS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivingroomS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
