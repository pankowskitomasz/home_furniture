import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LivingroomRoutingModule } from './livingroom-routing.module';
import { LivingroomComponent } from './livingroom/livingroom.component';
import { LivingroomS1Component } from './livingroom-s1/livingroom-s1.component';
import { LivingroomS2Component } from './livingroom-s2/livingroom-s2.component';


@NgModule({
  declarations: [
    LivingroomComponent,
    LivingroomS1Component,
    LivingroomS2Component
  ],
  imports: [
    CommonModule,
    LivingroomRoutingModule
  ]
})
export class LivingroomModule { }
