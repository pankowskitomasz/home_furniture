import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LivingroomS2Component } from './livingroom-s2.component';

describe('LivingroomS2Component', () => {
  let component: LivingroomS2Component;
  let fixture: ComponentFixture<LivingroomS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LivingroomS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LivingroomS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
