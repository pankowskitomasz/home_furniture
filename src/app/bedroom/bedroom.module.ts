import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BedroomRoutingModule } from './bedroom-routing.module';
import { BedroomComponent } from './bedroom/bedroom.component';
import { BedroomS1Component } from './bedroom-s1/bedroom-s1.component';
import { BedroomS2Component } from './bedroom-s2/bedroom-s2.component';


@NgModule({
  declarations: [
    BedroomComponent,
    BedroomS1Component,
    BedroomS2Component
  ],
  imports: [
    CommonModule,
    BedroomRoutingModule
  ]
})
export class BedroomModule { }
