import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BedroomS2Component } from './bedroom-s2.component';

describe('BedroomS2Component', () => {
  let component: BedroomS2Component;
  let fixture: ComponentFixture<BedroomS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BedroomS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
