import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BedroomS1Component } from './bedroom-s1.component';

describe('BedroomS1Component', () => {
  let component: BedroomS1Component;
  let fixture: ComponentFixture<BedroomS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BedroomS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
