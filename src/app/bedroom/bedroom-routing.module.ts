import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BedroomComponent } from './bedroom/bedroom.component';

const routes: Routes = [
  {
    path: "bedroom",
    component: BedroomComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BedroomRoutingModule { }
