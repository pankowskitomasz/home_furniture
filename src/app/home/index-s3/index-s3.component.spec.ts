import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexS3Component } from './index-s3.component';

describe('IndexS3Component', () => {
  let component: IndexS3Component;
  let fixture: ComponentFixture<IndexS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
