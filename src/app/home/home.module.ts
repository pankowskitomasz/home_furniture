import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { IndexS1Component } from './index-s1/index-s1.component';
import { IndexS2Component } from './index-s2/index-s2.component';
import { IndexS3Component } from './index-s3/index-s3.component';


@NgModule({
  declarations: [
    HomeComponent,
    IndexS1Component,
    IndexS2Component,
    IndexS3Component
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
