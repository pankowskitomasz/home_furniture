import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexS2Component } from './index-s2.component';

describe('IndexS2Component', () => {
  let component: IndexS2Component;
  let fixture: ComponentFixture<IndexS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
