import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KitchenRoutingModule } from './kitchen-routing.module';
import { KitchenComponent } from './kitchen/kitchen.component';
import { KitchenS1Component } from './kitchen-s1/kitchen-s1.component';
import { KitchenS2Component } from './kitchen-s2/kitchen-s2.component';


@NgModule({
  declarations: [
    KitchenComponent,
    KitchenS1Component,
    KitchenS2Component
  ],
  imports: [
    CommonModule,
    KitchenRoutingModule
  ]
})
export class KitchenModule { }
