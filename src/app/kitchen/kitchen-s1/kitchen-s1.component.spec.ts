import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenS1Component } from './kitchen-s1.component';

describe('KitchenS1Component', () => {
  let component: KitchenS1Component;
  let fixture: ComponentFixture<KitchenS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KitchenS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
