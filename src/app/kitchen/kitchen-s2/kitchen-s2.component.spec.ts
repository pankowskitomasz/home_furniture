import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KitchenS2Component } from './kitchen-s2.component';

describe('KitchenS2Component', () => {
  let component: KitchenS2Component;
  let fixture: ComponentFixture<KitchenS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KitchenS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KitchenS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
